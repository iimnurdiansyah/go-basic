package main

import "fmt"

func main() {


	//deklarasi variabel tipe 1
	// langsung di diset tipe datanya dan value nya
	var student1 string = "Djarod"
	// end

	// deklarasi variabel tipe 2, define nama dan tipenya baru value nya
    var student2 string
	student2 = "Jerino"
	// end


	// deklarasi variable tipe 3, tanpa kasih tipe data, pakai :=
	// yang jadi catatan adalah, pakai := hanya di awal, ketika ingin ubah nilai, pakai = aja
	student3 := "yanto"
	//student3 := "nur" //salah
	student3 =" Momo" //benar
	// end

	// deklarasi  multi variable
	var student7, student8 string= "hasan", "deni"
	student4, student5, student6 := "murni", "jaya", "wawan"
	// end


	// variable underscore, variable untuk nampung nilai sampah. gak bisa dicetal
	_ = "iim"
	// end


	// tipe tipe cetak print
	fmt.Printf("halo ini pake printf %s %s %s!\n", student1, student2,student3)
	fmt.Println("halo ini pake println ", student1, student2,student3 + "! \n")
	fmt.Printf("halo ini pake printf %s %s %s!\n", student4, student5,student6)
	fmt.Printf("halo ini pake printf %s %s !\n", student7, student8)
	//end


}
// https://dasarpemrogramangolang.novalagung.com/8-variabel.html