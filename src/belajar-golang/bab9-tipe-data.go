package main

import "fmt"

func main() {


	//data number
	var positiveNumber uint8 = 89
	var negativeNumber = -1243423644
	var decimalNumber = 2.62

	fmt.Printf("bilangan desimal: %f\n", decimalNumber)
	fmt.Printf("bilangan desimal: %.3f\n", decimalNumber)	
	fmt.Printf("bilangan positif: %d\n", positiveNumber)
	fmt.Printf("bilangan negatif: %d\n", negativeNumber)



	// data boolean
	var exist bool = true
	fmt.Printf("exist? %t \n", exist)


	// data string
	var message = `Nama saya "John Wick".
	Salam kenal.
	Mari belajar "Golang".`

	fmt.Println(message)
}

// https://dasarpemrogramangolang.novalagung.com/9-tipe-data.html